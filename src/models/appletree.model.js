import Debug    from 'debug';

import AppleTree from './schemas/appletree.schema';

const debug = Debug('appletree:model');

export default class AppleTreeModel {
    constructor() {
              
    }

    // Creates a new Apple Tree
    create(appleTreeData) {
        return new Promise((resolve, reject) => {
            const appleTree = new AppleTree(appleTreeData);
            appleTree.save(err => {
                if(err){
                    debug('Error @models/appletree.model create(): ', err);
                    reject('Oops something went wrong!');
                }else{
                    debug('Created new apple tree: ' + appleTree._id);
                    resolve(appleTree);
                }
            });
        });
    };
    

    // Get all Apple Trees
    get(skip=0, limit=20, sort='-created') {
        return new Promise((resolve, reject) => {
            AppleTree.find({ 
                deleted: false 
            })
            .sort(sort)
            .skip(skip)
            .limit(limit)
            .exec((err, appleTrees) => {
                if(err){
                    debug('Error @models/appletree.model get(): ', err);
                    reject('Oops something went wrong!');
                }else{
                    debug('Get all apple trees: ' + appleTrees.length);
                    resolve(appleTrees);
                }
            });
        });
    };


    // Get one Apple Tree by id
    getById(id) {
        return new Promise((resolve, reject) => {
            AppleTree.findById(id, (err, appleTree) => {
                if(err){
                    debug('Error @models/appletree.model getById(): ', err);
                    reject('Oops something went wrong!');
                }else{
                    if(!appleTree){
                        reject('There is no apple tree for that id!');
                    }else{
                        debug('Get apple tree by id ', appleTree._id);
                        resolve(appleTree); 
                    }
                }
            });
        });
    };


    // Update one Apple Tree by id
    update(id, appleTreeToUpdate) {
        return new Promise((resolve, reject) => {

            // Fast way to protect keys from updating
            delete appleTreeToUpdate._id; // Dont update _id key
            delete appleTreeToUpdate.created; // Dont update created key
            delete appleTreeToUpdate.__v; // Dont update __v key
            delete appleTreeToUpdate.deleted; // Dont update deleted key

            AppleTree.findOneAndUpdate({ _id: id }, appleTreeToUpdate, { new: true }, (err, appleTree) => {
                if(err){
                    debug('Error @models/appletree.model update(): ', err);
                    reject('Oops something went wrong!');
                }else{
                    debug('Update an apple tree by id ', appleTree._id);
                    resolve(appleTree);
                }
            });
        });
    };


    // Updated 'deleted' key of Apple Tree by id
    deleteUpdate(id) {
        return new Promise((resolve, reject) => {
            AppleTree.findOneAndUpdate({ _id: id }, {'deleted' : true}, { new: true }, (err, appleTree) => {
                if(err){
                    debug('Error @models/appletree.model update(): ', err);
                    reject('Oops something went wrong!');
                }else{
                    debug('Update or Create an apple tree by id ', appleTree._id);
                    resolve(appleTree);
                }
            });
        });
    };

    // Delete an Apple Tree by id
    delete(id) {
        return new Promise((resolve, reject) => {
            AppleTree.remove({ _id: id }, (err, appleTree) => {
                if (err) {
                    debug('Error @models/appletree.model delete(): ', err);
                    reject('Oops something went wrong!');
                } else {
                    debug('Delete an apple tree by id ', id);
                    resolve(appleTree);
                }
            });
        })
    };
}