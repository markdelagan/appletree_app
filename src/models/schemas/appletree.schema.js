import mongoose from 'mongoose';

const AppleTreeSchema = new mongoose.Schema({
	name:       { type: String, default: 'Apple Tree'},
    deleted:    { type: Boolean, default: false },
    created:    { type: Date }
});

AppleTreeSchema.pre('save', function(next) {
    const now = new Date();
    if (!this.created) {
        this.created = now;
    }
    next();
});


AppleTreeSchema.methods.hasDefaultName = function(){
	return this.name;
}

AppleTreeSchema.methods.hasDefaultDeleted = function(){
	return this.deleted;
}

const AppleTree = mongoose.model('apple_tree', AppleTreeSchema);

export default AppleTree;