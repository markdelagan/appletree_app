# Apple tree App

## Description

This is a simple CRUD application. You can communicate with this app through CURL, since there is no UI to interact with. 

This Node.js application will create an Apple Tree with a default name "Apple Tree". You can create an Apple Tree with a custom name via POST. Update apple tree's "name". There are two delete alternatives: 

- a soft delete where "deleted" key set to true
- a hard delete where the document is deleted from DB

## RUN

For testing: `npm run test`
To start the application: `npm run start`

You can see in package.json the scripts. ES6 app is built into the file 'dist' where ES5 code is generated and executed for production.

## API

- POST		/api/v1/appletree
- GET 		/api/v1/appletree
- GET 		/api/v1/appletree/:id
- UPDATE 	/api/v1/appletree/:id
- DELETE 	/api/v1/appletree/:id
- DELETE 	/api/v1/appletree/delete/:id

## Backend

I used for backend Express and MongoDb with Mongoose. 
I like to open a database connection and reuse it insted of open and close a connection for each databse request.

## Error handling

I use an "Oops something went wrong" as promise reject message. That's because I dont like to show the user the real server error.
I use to log all errors and send to the client a generic error message.

## Things missing in this project

- Server run file
- Server listeners
- Server error handling
- Normalize Server port
- Use of pm2 or forever for restarting server
- Tests coverage like codacy for code quality
- Proper/deeper unit/integration testing 

## AWS

http://54.145.69.11:3000