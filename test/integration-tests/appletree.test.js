import chai from 'chai';
import chaiHttp from 'chai-http';
import mongoose from 'mongoose';

import AppleTreeModel from '../../src/models/appletree.model';

const expect = chai.expect;
const appleTreeModel = new AppleTreeModel();

chai.use(chaiHttp);

describe('Apple Tree Integration Testing', () => {
	describe('GET /', () => {
		it('Data is normalized', done => {
			chai
			.request('http://localhost:3000')
			.get('/')
			.then((res) => {
				expect(res).to.have.status(200);
				expect(res).to.be.an('object');
				done();
			});
		});
	});


	describe('POST /api/v1/appletree/', () => {
		it('Create an apple tree', done => {
			chai
			.request('http://localhost:3000')
			.post('/api/v1/appletree')
			.then((res) => {
				expect(res).to.have.status(200);
				expect(res).to.be.an('object');
				done();
			});
		});
	});

	describe('GET /api/v1/appletree/', () => {
		it('Get all apple trees', done => {
			chai
			.request('http://localhost:3000')
			.get('/api/v1/appletree')
			.then((res) => {
				expect(res).to.have.status(200);
				expect(res.body).to.be.an('array');
				done();
			});
		});
	});

	
	describe('GET /api/v1/appletree/:id', () => {
		it('Get an apple tree by id', done => {
			appleTreeModel.create()
			.then((appleTree) => {
				chai
				.request('http://localhost:3000')
				.get(`/api/v1/appletree/${appleTree._id}`)
				.then((res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.be.an('object');
					done();
				});
			});
		});

	});

	describe('PUT /api/v1/appletree/', () => {
		it('Update an apple tree by id', done => {
			appleTreeModel.create()
			.then((appleTree) => {
				chai
				.request('http://localhost:3000')
				.put(`/api/v1/appletree/${appleTree._id}`)
				.send({ name : 'Manzano' })
				.then((res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.be.an('object');
					expect(res.body.name).to.be.equal('Manzano');
					done();
				});
			});
		});
	});

	describe('SOFT DELETE /api/v1/appletree/', () => {
		it('Soft Delete an apple tree by id', done => {
			appleTreeModel.create()
			.then((appleTree) => {
				chai
				.request('http://localhost:3000')
				.delete(`/api/v1/appletree/${appleTree._id}`)
				.then((res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.be.an('object');
					expect(res.body).to.have.property('deleted').to.be.true;
					done();
				});
			});
		});
	});

	describe('DELETE /api/v1/appletree/', () => {
		it('Delete an apple tree by id', done => {
			appleTreeModel.create()
			.then((appleTree) => {
				chai
				.request('http://localhost:3000')
				.delete(`/api/v1/appletree/delete/${appleTree._id}`)
				.then((res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.be.an('object');
					expect(res.body).to.have.property('n').to.be.equal(1);
					expect(res.body).to.have.property('ok').to.be.equal(1);
					done();
				});
			});
		});
	});

	after(()=>{
        // Drop database after testing
        mongoose.connection.dropDatabase();
    });

});