import express from 'express';
import AppleTreeController from '../controllers/appletree.controller';

const router 				= express.Router();
const appleTreeController 	= new AppleTreeController();

router.post('/',      		appleTreeController.create);   		// Create a new apple tree
router.get('/',       		appleTreeController.get);      		// Get all apple trees
router.get('/:id',    		appleTreeController.getById);  		// Get apple tree by id
router.put('/:id',    		appleTreeController.update);      	// Update / Create apple tree
router.delete('/:id', 		appleTreeController.deleteUpdate);  // Set 'deleted' key from apple tree to true by id
router.delete('/delete/:id',appleTreeController.delete);   		// Delete apple tree by id

export default router;