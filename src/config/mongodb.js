import mongoose from 'mongoose';
import Debug 	from 'debug';

const debug = Debug('mongodb');

export default class MongoDB { 
	constructor(){
		//console.log(process.env.NODE_ENV);
		mongoose.Promise 	= global.Promise;

		if(process.env.NODE_ENV == 'development') {
            this.mongoUrl = process.env.MONGODB_URI_DEVELOPMENT;
        }else if(process.env.NODE_ENV == 'production') {
            this.mongoUrl = process.env.MONGODB_URI_PRODUCTION;
        }else if(process.env.NODE_ENV == 'testing') {
            this.mongoUrl = process.env.MONGODB_URI_TESTING;
        }
	}
	
	start(){
		let db = mongoose.connect(this.mongoUrl, err => {
			if(err){
				debug('Error @config/mongodb start(): ', err);
			}
		});
	}
}
