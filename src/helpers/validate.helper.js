import validator from 'validator';
import Debug from 'debug';

const debug = Debug('helpers:validate');

export default {
	// Validate MongoDB ID
	mongoId: (id) => {
		return new Promise((resolve, reject) => {
			if(validator.isMongoId(id) === true){
				debug('Valid Mongo ID');
				resolve();
			}else{
				debug('Error @helpers/validate.helper mongoId(): Invalid Mongo ID ', id);
				reject();
			}
		});
	},

	// Filter req.body to only apply name if exists
	// This prevents inyection of other data
	filterForName: (body) => {
		return new Promise((resolve, reject) => {
            let customName = { 'name' : null };
            
            if(body.name){
                customName.name = body.name;
                resolve(customName);
            }else{
                resolve(body.name);
            }
            
        });
	}

}