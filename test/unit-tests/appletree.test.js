import chai from 'chai';
import mongoose from 'mongoose';
import chaiAsPromise from 'chai-as-promised';

// Run server and Mongo
import server from '../../src/index';

import AppleTreeSchema from '../../src/models/schemas/appletree.schema';
import AppleTreeModel from '../../src/models/appletree.model';

const expect = chai.expect;

const appleTreeSchema = new AppleTreeSchema();
const appleTreeModel = new AppleTreeModel();

chai.use(chaiAsPromise);


describe('Apple Tree Unit Testing', () => {

    describe('Apple Tree SCHEMA', () => {

        it('Apple tree to return an object', () => {
            expect(appleTreeSchema).to.be.an('object');
        });

        it('Apple tree has default name', () => {
            const actual = appleTreeSchema.hasDefaultName();
            expect(actual).to.be.equal('Apple Tree');
        });

        it('Default name is a string', () => {
            const actual = appleTreeSchema.hasDefaultName();
            expect(actual).to.be.a('string');
        });

        it('"deleted" is a boolean', () => {
            const actual = appleTreeSchema.hasDefaultDeleted();
            expect(actual).to.be.a('boolean');
        });

        it('"deleted" default value is false', () => {
            
            const actual = appleTreeSchema.hasDefaultDeleted();
            expect(actual).to.be.false;
        });

        it('Create apple tree with custom name', () => {
            const customSchema = new AppleTreeSchema({
                'name': 'Manzano'
            });
            const custom = customSchema.hasDefaultName();
            expect(custom).to.be.equal('Manzano');
        });

    });



    describe('Apple tree CREATE', () => {

        it('Create new apple tree and save to DB', (done) =>{
            appleTreeModel.create()
            .then((appleTree) => {
                expect(appleTree).to.be.an('object');
                done();
            })
        });

        
        it('Check if default name is "Apple Tree"', (done) =>{
            appleTreeModel.create()
            .then((appleTree) => {
                expect(appleTree.name).to.be.equal('Apple Tree');
                done();
            })
        });

        it('"created" key is a Date', (done) =>{
            appleTreeModel.create()
            .then((appleTree) => {
                expect(appleTree.created).to.be.a('date');
                done();
            })
        });

        it('"deleted" key is a boolean', (done) =>{
            appleTreeModel.create()
            .then((appleTree) => {
                expect(appleTree.deleted).to.be.a('boolean');
                done();
            })
        });

        it('"deleted" key is by default false', (done) =>{
            appleTreeModel.create()
            .then((appleTree) => {
                expect(appleTree.deleted).to.be.false;
                done();
            })
        });

        it('Create new apple tree with custom name "Manzano"', (done) =>{
            const newAppleTree = {
                'name': 'Manzano'
            };

            appleTreeModel.create(newAppleTree)
            .then((appleTree) => {
                expect(appleTree.name).to.be.equal('Manzano');
                done();
            })
        });

        it('Custom names are returned always as strings', (done) =>{
            const newAppleTree = {
                'name': 899
            };

            appleTreeModel.create(newAppleTree)
            .then((appleTree) => {
                expect(appleTree.name).to.be.a('string');
                done();
            })
        });


        

        after(()=>{
            // Drop database after testing
            mongoose.connection.dropDatabase();
        });

    });

    describe('Apple tree GET ALL', () => {

        it('Is returning an array', (done) =>{
            appleTreeModel.create()
            .then(() => appleTreeModel.create())
            .then(() => appleTreeModel.create())
            .then(() => appleTreeModel.get())
            .then((appleTree) => {
                expect(appleTree).to.be.an('array');
                done();
            })
        });

        it('Create 3 apple trees and get an array with length 3', (done) =>{
            appleTreeModel.create()
            .then(() => appleTreeModel.create())
            .then(() => appleTreeModel.create())
            .then(() => appleTreeModel.get())
            .then((appleTree) => {
                expect(appleTree).to.have.lengthOf(3);
                done();
            })
        });

        afterEach(()=>{
            // Drop database after testing
            mongoose.connection.dropDatabase();
        });

    });


    describe('Apple tree GET BY ID', () => {

        it('Is returning an object', (done) =>{
            appleTreeModel.create()
            .then((appleTree) => appleTreeModel.getById(appleTree._id))
            .then((appleTree) => {
                expect(appleTree).to.be.an('object');
                done();
            })
        });

        it('Is returning the correct document', (done) =>{
            let id; // Id we are requesting
            appleTreeModel.create()
            .then((appleTree) => {
                id = appleTree._id;
                return appleTreeModel.getById(appleTree._id);
            })
            .then((appleTree) => {
                expect(appleTree._id).to.be.eql(id);
                done();
            })
        });
    
        it('With a invalid id', () => {
            expect(appleTreeModel.getById('000')).to.be.rejectedWith('Oops something went wrong!');
        });

        it('With non existing id', () => {
            expect(appleTreeModel.getById('5886b8fb226e2202622c65e7')).to.be.rejectedWith('There is no apple tree for that id!');
        });

        afterEach(()=>{
            // Drop database after testing
            mongoose.connection.dropDatabase();
        });

    });


    describe('Apple tree UPDATE', () => {

        it('Is returning an object', (done) => {
            appleTreeModel.create()
            .then((appleTree) => appleTreeModel.update(appleTree._id, appleTree))
            .then((appleTree) => {
                expect(appleTree).to.be.an('object');
                done();
            })
        });

        it('Is updating the correct document', (done) =>{
            let id; // Id we are requesting
            let name;

            appleTreeModel.create()
            .then((appleTree) => {
                id = appleTree._id;
                return appleTreeModel.update(appleTree._id, appleTree);
            })
            .then((appleTree) => {
                expect(appleTree._id).to.be.eql(id);
                done();
            })
        });

        it('Is updating the right value', (done) =>{
            let update = { name: "Manzano" };

            appleTreeModel.create()
            .then((appleTree) => {
                return appleTreeModel.update(appleTree._id, update);
            })
            .then((appleTree) => {
                expect(appleTree.name).to.be.eql(update.name);
                done();
            })
        });

        afterEach(()=>{
            // Drop database after testing
            mongoose.connection.dropDatabase();
        });

    });


    describe('Apple tree SOFT DELETE', () => {

        it('Is returning an object', (done) => {
            appleTreeModel.create()
            .then((appleTree) => appleTreeModel.deleteUpdate(appleTree._id))
            .then((appleTree) => {
                expect(appleTree).to.be.an('object');
                done();
            })
        });

        it('Is soft deleting the correct document', (done) =>{
            let id; // Id we are requesting

            appleTreeModel.create()
            .then((appleTree) => {
                id = appleTree._id;
                return appleTreeModel.deleteUpdate(appleTree._id);
            })
            .then((appleTree) => {
                expect(appleTree._id).to.be.eql(id);
                expect(appleTree.deleted).to.be.true;
                done();
            })
        });


        afterEach(()=>{
            // Drop database after testing
            mongoose.connection.dropDatabase();
        });

    });


    describe('Apple tree DELETE', () => {

        it('Is returning an object', (done) => {
            appleTreeModel.create()
            .then((appleTree) => appleTreeModel.delete(appleTree._id))
            .then((appleTree) => {
                expect(appleTree).to.be.an('object');
                done();
            })
        });

        it('Is deleting the document', (done) =>{
            let id; // Id we are requesting

            appleTreeModel.create()
            .then((appleTree) => {
                id = appleTree._id;
                return appleTreeModel.delete(appleTree._id);
            })
            .then((appleTree) => {
                expect(appleTree.result).to.have.property('n').to.be.eql(1);
                expect(appleTree.result).to.have.property('ok').to.be.eql(1);
                done();
            })
        });

        afterEach(()=>{
            // Drop database after testing
            mongoose.connection.dropDatabase();
        });

    });

});
