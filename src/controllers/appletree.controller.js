import Debug from 'debug';

import AppleTreeModel from '../models/appletree.model'
import validate from '../helpers/validate.helper'

const debug = Debug('appletree:controller');
const appleTreeModel = new AppleTreeModel();

export default class AppleTreeController {

    constructor(){

    }
    
    // Creates a new Apple Tree
    create(req, res, next) {
        validate.filterForName(req.body)
        .then(customName => appleTreeModel.create(customName))
        .then(appleTree => res.json(appleTree))
        .catch(error => {
            debug('Error @controllers/appletree.controller create(): ', error);
            res.status(500).send(`{ "error": "${error}" }`);
        });
    };


    // Get all Apple Trees
    get(req, res, next) {
        const skip  = req.params.skip;
        const limit = req.params.limit;
        const sort  = req.params.sort;

        appleTreeModel.get(skip, limit, sort)
        .then(appleTrees => res.json(appleTrees))
        .catch(error => {
            debug('Error @controllers/appletree.controller get(): ', error);
            res.status(500).send(`{ "error": "${error}" }`);
        });
    };

    
    // Get one Apple Tree by id
    getById(req, res, next) {
        validate.mongoId(req.params.id)
        .then(() => appleTreeModel.getById(req.params.id))
        .then(appleTree => res.json(appleTree))
        .catch( error => {
            debug('Error @controllers/appletree.controller getById(): ', error);
            res.status(404).send(`{ "error":  "${error}" }`);
        });
    };

    // Update one Apple Tree by id
    update(req, res, next) {
        validate.mongoId(req.params.id)
        .then(() => appleTreeModel.update(req.params.id, req.body))
        .then(appleTree => res.json(appleTree))
        .catch(error => {
            debug('Error @controllers/appletree.controller update(): ', error);
            res.status(500).send(`{ "error": "${error}" }`);
        });
    };

    // Updated 'deleted' key of Apple Tree by id
    deleteUpdate(req, res, next) {
        validate.mongoId(req.params.id)
        .then(() => appleTreeModel.deleteUpdate(req.params.id))
        .then(appleTree => res.json(appleTree))
        .catch(error  => {
            debug('Error @controllers/appletree.controller deleteUpdate(): ', error);
            res.status(500).send(`{ "error": "${error}" }`);
        });
    };

    // Delete an Apple Tree by id
    delete(req, res, next) {
        validate.mongoId(req.params.id)
        .then(() => appleTreeModel.delete(req.params.id))
        .then(appleTree => res.json(appleTree))
        .catch(error  => {
            debug('Error @controllers/appletree.controller delete(): ', error);
            res.status(500).send(`{ "error": "${error}" }`);
        });
    };
    
}