import express      from 'express';
import path         from 'path';
import logger       from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser   from 'body-parser';
import http         from 'http';
import dotenv       from 'dotenv';

dotenv.config(); //.env environment variables

import routes from './routes/api.routes';
import MongoServer  from './config/mongodb';

const mongoServer = new MongoServer();

const app = express();
const server = http.createServer(app);
//-  VIEW ENGINE SETUP
//app.set('views', path.join(__dirname, '../' , 'views'));
app.set('view engine', 'pug');

//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//*** ROUTING ***
app.use(`/api/v1/appletree`, routes);
app.get("/", (req, res) => {
    res.json('Welcome to an apple tree CRUD, please use CURL to use this RESTful app');
});

//-  catch 404 and forward to error handler
app.use((req, res, next)  => {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//-  ERROR HANDLERS
//-  development error handler
//-  will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

//-  production error handler
//-  no stacktraces leaked to user
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

server.listen(process.env.PORT || 3000, () => {
    mongoServer.start();
    console.log('Magic is happening on port 3000');
});

export default server;